import 'dart:async';
import 'package:Libbitcoin_Experiments/base.dart' as b;
import 'package:Libbitcoin_Experiments/classes.dart' as c;


Future<c.Wallet> generate_wallet(known_seed, group_number, {use_group_number=false}) async {
  // Initialize the seed value
  String seed;

  while (true) {
    // Step 1: Generate seed
    if (known_seed == '') {
      seed = await b.execute_process(['seed']);
    } else {
      seed = known_seed;
    }

    // Step 2: Generate the private key
    String private_key = await b.execute_process(['ec-new', seed]);

    // Step 3: Generate the public key
    String public_key = await b.execute_process(['ec-to-public', private_key]);

    // Step 4: Fetch the public key ending
    String public_key_ending = public_key.substring(public_key.length - group_number.length, public_key.length);

    if (public_key_ending == group_number || known_seed != '' || use_group_number == false) {
      // Step 5: Generate the corresponding addresses
      String address = await b.execute_process(['ec-to-address', '-v', '111', public_key]);

      // Step 6: Turn the generated data into Wallet objects
      c.Wallet wallet = c.Wallet(seed, private_key, public_key, address);

      // Return the wallet object
      return wallet;
    }
  }
}
