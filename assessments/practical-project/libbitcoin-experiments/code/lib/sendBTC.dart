import 'dart:async';
import 'dart:convert' as JSON;

import 'base.dart' as b;
import 'classes.dart' as c;


void sendBTC(sender, receiver, amount, fee, percent_paying, {execute_tx: false, basic_payment: false, op_return: false, multisignatures: false, ntxs: 1, encoded_script: null, multisig: null}) async {
  // Initialize variables
  int required_funds = amount + fee;
  int success_counter = 0;
  
  // List of usable txs
  List<c.Transaction> txs;
  if (!multisignatures) {
    txs = await fetchUsableTxs(sender, required_funds, percent_paying);
  } else {
    txs = await fetchUsableTxs(multisig, required_funds, percent_paying);
  }

  // Loop for each new tx
  for (var i = 0; i < txs.length; i++) {
    if (success_counter < ntxs) {
      c.Output output = await fetchTransaction(txs[i]);
      
      String tx_encode;
      if (basic_payment) {
        tx_encode = await encodeTx(sender, receiver, txs[i], amount, fee, percent_paying);
      } else if (op_return) {
        tx_encode = await encodeTx(sender, receiver, txs[i], amount, fee, percent_paying, encoded_script: encoded_script);
      } else if (multisignatures) {
        tx_encode = await encodeTx(sender, receiver, txs[i], amount, fee, percent_paying, multisignatures: true);
      }

      if (multisignatures) {
        String endorsement_1 = await b.execute_process(['input-sign', sender.private_key, multisig.multisig_cmd[1], tx_encode]);
        String endorsement_2 = await b.execute_process(['input-sign', receiver.private_key, multisig.multisig_cmd[1], tx_encode]);
        String script_encode = await b.execute_process(['script-encode', multisig.multisig_cmd[1]]);
        String set_input = await b.execute_process(['input-set', 'zero [' + endorsement_1 + '] [' + endorsement_2 + '] [' + script_encode + ']', tx_encode]);
        String tx_decode = JSON.jsonDecode(await b.execute_process(['tx-decode', '-f', 'json', set_input]))['transaction']['hash'];
        
        if (execute_tx) {
          String send_tx = await b.execute_process(['send-tx', set_input]);
          print(send_tx + ' Tx hash: ' + tx_decode);
        } else {
          print('Transaction is correct, but has not been sent!');
          // Iterate number of successful transactions
          success_counter += 1;
        }
      } else {

        String endorsement = await b.execute_process(['input-sign', sender.private_key, output.script, tx_encode]);
        
        String set_input = await b.execute_process(['input-set', '[' + endorsement + '] [' + sender.public_key + ']', tx_encode]);
        
        String tx_decode = JSON.jsonDecode(await b.execute_process(['tx-decode', '-f', 'json', set_input]))['transaction']['hash'];
        
        String validate_input = await b.execute_process(['input-validate', sender.public_key, output.script, endorsement, set_input]);

        if (validate_input == 'The endorsement is valid.') {
          String validate_tx = await b.execute_process(['validate-tx', set_input]);

          if (validate_tx == 'The transaction is valid.') {
            if (execute_tx) {
              String send_tx = await b.execute_process(['send-tx', set_input]);
              print(send_tx + ' Tx hash: ' + tx_decode);
            } else {
              print('Transaction is correct, but has not been sent!');
            }
            // Iterate number of successful transactions
            success_counter += 1;
          } else {
            print('Transaction is invalid: ' + validate_tx);
          }
        } else {
          print('Endorsement is invalid: ' + validate_input);
        }
      }
    }
  }
  // Notification message to user (console)
  if (success_counter != ntxs) {
    print('Not all requested transactions were sent. Only ' + success_counter.toString() + '/' + ntxs.toString() + ' has been transferred.');
  }
}


void fetchHistory(wallet) async {
  // Initialize list to hold transactions
  List<c.Transaction> txs = new List<c.Transaction>();

  // Fetch transaction history
  var transfers = JSON.jsonDecode(await b.execute_process(['fetch-history', '-f', 'json', wallet.address]))['transfers'];

  // Loop through transactions
  for (var tx in transfers) {
    // Check if current transaction has been spent
    if (tx['spent'] == null) {
      // Create transaction object
      c.Transaction current_tx = c.Transaction(
        tx['received']['hash'],
        int.parse(tx['received']['height']),
        int.parse(tx['received']['index']),
        int.parse(tx['value'])
      );
      // Add transaction to list of transactions
      txs.add(current_tx);
    }
  }
  // Update wallet transactions
  wallet.updateTxs(txs);
}


Future<List<c.Transaction>> fetchUsableTxs(wallet, required_funds, percent_paying) async {
  // Initialize list of txs
  List<c.Transaction> usable_txs = new List<c.Transaction>();

  // Loop through txs and check requirements
  for (var tx in wallet.txs) {
    if (percent_paying) {
      usable_txs.add(tx);
    } else {
      if (tx.value >= required_funds) {
          usable_txs.add(tx);
      }
    }
  }
  // Return found txs
  return usable_txs;
}


Future<c.Output> fetchTransaction(c.Transaction tx) async {
  // Fetch transaction response
  var transaction = JSON.jsonDecode(await b.execute_process(['fetch-tx', '-f', 'json', tx.tx_hash]))['transaction'];

  // Select the output elements as list
  List outputs = transaction['outputs'];

  // Loop through outputs
  for (var output in outputs) {
    int value = int.parse(output['value']);

    if (value == tx.value) {
      return c.Output(output['address_hash'], output['script'], int.parse(output['value']));
    }
  }
  return null;
}


Future<String> encodeTx(sender, receiver, tx, amount, fee, percent_paying, {encoded_script: null, multisignatures: false}) async {
  int pay_amount;
  int pay_fee;
  int pay_remaining;

  if (percent_paying) {
    pay_amount    = ((tx.value / 100) * amount).round();
    pay_fee       = ((tx.value / 100) * fee).round();
    pay_remaining = tx.value - pay_amount - pay_fee;
  } else {
    pay_amount    = amount;
    pay_fee       = fee;
    pay_remaining = tx.value - pay_amount - pay_fee;
  }

  // Multisignature transaction
  if (multisignatures) {
    pay_amount    = (pay_amount / 2).round();
    pay_remaining = (pay_amount / 2).round();
  }

  List<String> updated_commands;
  List<String> commands = ['tx-encode', '-i', tx.tx_hash + ':0', '-o', receiver.address + ':' + pay_amount.toString()];

  if (pay_remaining > 0) {
    List<String> remaining = ['-o', sender.address + ':' + pay_remaining.toString()];
    updated_commands = new List.from(commands)..addAll(remaining);
  } else {
    updated_commands = commands;
  }

  // Adding OP RETURN statement as output
  if (encoded_script != null) {
    List<String> remaining = ['-o', encoded_script + ':0'];
    updated_commands = new List.from(commands)..addAll(remaining);
  }

  return await b.execute_process(updated_commands);
}
