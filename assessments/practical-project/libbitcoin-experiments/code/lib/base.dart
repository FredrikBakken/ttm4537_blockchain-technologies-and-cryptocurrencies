import 'dart:io';
import 'dart:async';

import 'package:Libbitcoin_Experiments/settings.dart' as s;


Future<String> execute_process(commands) async {
  // Initialize variables
  List<String> config;
  List<String> updated_commands;

  // Extend commands with testnet config
  if (s.testnet) {
    config = ['-c', 'bx/bx.cfg'];
    updated_commands = new List.from(commands)..addAll(config);
  } else {
    updated_commands = commands;
  }

  // Execute process
  ProcessResult output = await Process.run('bx/bx.exe', updated_commands);

  // Format results
  String result = (output.stdout).trim();

  // Fetch output error
  if (result == '') {
    result = (output.stderr).trim();
  }

  // Return results
  return result;
}
