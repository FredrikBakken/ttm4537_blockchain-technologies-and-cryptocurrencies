# TTM4537: Blockchain Technologies and Cryptocurrencies

The course covers the cryptographic theory supporting Bitcoin and other cryptocurrencies as well as the practical aspects of how a cryptocurrency is designed. The advantages and disadvantages of different approaches will be explored. More general applications of blockchain technologies will also be included.


## Timetable

| **Day**   | **Time**      | **Type** | **Room**                               |
|-----------|---------------|----------|----------------------------------------|
| Tuesday   | 08:15 - 10:00 | Lecture  | (S1) Sentralbygg 1                     |
| Wednesday | 12:15 - 14:00 | Exercise | (A3) Handelshøyskolen / IIK Sahara Lab |


## Examination

| **Evaluation Type** | **Weighting** | **Aids** | **Date**   | **Time**      |
|---------------------|---------------|----------|------------|---------------|
| Online quizzes      | 15%           | A        |            |               |
| Pratical task       | 25%           | A        |            |               |
| Written exam        | 60%           | D        | 29.11.2018 | 09:00 - 12:00 |