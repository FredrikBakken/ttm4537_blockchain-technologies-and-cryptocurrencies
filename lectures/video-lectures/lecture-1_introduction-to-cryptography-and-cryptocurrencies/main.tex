\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}


\begin{document}

\section{Intro to Cryptography and Cryptocurrencies}

\subsection{Cryptographic Hash Functions}

A cryptographic hash function is a mathematical function with three attributes:
\begin{enumerate}
    \item Takes any string as input 
    \item Fixed-size output (256-bits used in lectures)
    \item Efficiently computable
\end{enumerate}

\noindent In cryptocurrencies, having cryptographicly secure hash functions is a necessity. Some security properties that are needed are:
\begin{enumerate}
    \item \textbf{Collision-free}\\
    Nobody can find \textit{x} and \textit{y}, such that $ x != y $ and $ H(x) = H(y) $. In order words, it should not be possible to find two different values that can be hashed to the same output hash.

    Since there are no limitations for the format of the input string, while the output is limited to 256-bits, there must exist collisions. Therefore, collisions do exist, but can anyone find them?

    \textit{Example (How to find a collision?):} By selecting $ 2^{130} $ random inputs, there is a 99.8\% chance that two of them will collide. This works no matter what $ H $ (hash-function) is used, but it takes too long no matter what.

    Is there a faster way to find collisions? For some possible $ H $'s, yes. For others, we do not know of one. No $ H $ has been \underline{proven} collision-free.

    \textit{Application (Hash as message digest):} If we know $ H(x) = H(y) $, it is safe to assume that $ x = y $. To recognize a file that we saw before, just remember its hash. This is useful since the hash is small, compared to keeping the entire file in memory.

    \item \textbf{Hiding}\\
    The property that we want is, given $ H(x) $, it is infeasible to find $ x $. In other words, if given the output of the hash-function, there is no feasible way to find the input.

    \textit{Example (Heads and tails):} If someone knew the output hashes from "heads" and "tails", then they would easy be able to link which input goes to which output, since there are a small limited number of potential input values. As a result, it is necessary to pick $ x $ from a large unlimited pool of values, not a limited amount of values.

    \textit{The hiding property:} If $ r $ is chosen from a probability distribution that has \textit{high min-entropy}, then given $ H(r | x) $, it is infeasible to find $ x $. High min-entropy means that the distribution is "very spread out", so that no particular value is chosen with more than negligible probability.

    \textit{Exmaple (Hiding property):} If $ r $ is chosen uniformly from all of the strings that are 256-bits long, then any particular string was chosen with probability $ 1 / 2^{256} $, which is truely a negligible value.

    \textit{Example (Application):} Design a digital representation of a commitment application, where we want to "seal a value in an envelope" and "open the envelope" later. The value is then commited and revealed at a later time. Security properties of such an application is:
    \begin{itemize}
        \item Hiding: Given the commitment, it is infeasible to find the message.
        \item Biding: Infeasible to find message != message', such that\\ verify(commit(msg), msg') == true.
    \end{itemize}

    \item \textbf{Puzzle-friendly}\\
    For every possible output value $ y $, if $ k $ is chosen from a distribution with high min-entropy, then it is infeasible to find $ x $ such that $ H(k | x) = y $.

    \textit{Example (Search puzzel application):} Given a "puzzle ID" $ id $ (from high min-entropy distribution) and a target set $ Y $: Try to find a "solution" $ x $, such that $ H(id | x) \in Y $. Puzzle-friendly property implies that no solving strategy is much better than trying random values of $ x $.
\end{enumerate}

\subsubsection{SHA-256 Hash Function}

Bitcoin uses the SHA-256 hashing algorithm, which works by:

\begin{enumerate}
    \item Takes the input message and splits it into blocks of size 512 bits where padding is added at the end.
    \item The algorithm starts by taking the 256 bits IV value (number from a standards document) and the first message block and runs them through the function $ c $, which comutes a 256 bits output.
    \item All future iterations follows the same strucutre, by taking the output value from the previous $ c $ function and the next message block, through the $ c $ function.
    \item At the end, the output results the 256 bits hash.
\end{enumerate}

\noindent \underline{Theorem:} If $ c $ is collision-free, then SHA-256 is collision-free.

\noindent \includegraphics[width=12.2cm]{images/sha-256.png}


\subsection{Hash Pointers and Data Structures}

A hash pointer is a pointer to where some information is stored and a (cryptographic) hash of the information. Given a hash pointer, we can ask to get the information back and verify that it has not changed.\\

\noindent \underline{Key idea:} Build data structures with hash pointers.

\subsubsection{Detecting Tampering}

An attacker targets the data in one of the previous blocks. When performing the re-computing of the hash-value, the output is different from before and it is possible to conclude with that the data has been tampered with.\\

\noindent If an attacker on the other hand attacks both the data of one block and the hash pointer of the next block, then the data adds up. This means that the attacker has to attack all the previous hash pointers and the used hash pointer to be able to tamper with a data block.\\

\noindent In other words, if an attacker wants to tamper with the data in one of the blocks, he/she would have to tamper with all the hash pointers up to the beginning in order to succeed, but it will not be possible for the attacker to tamper with the hash pointer which we are using from the beginning. It is therefore possible for us to build a blockchain following these properties, with as many blocks as we'd like.


\subsubsection{Binary Tree with Hash Pointers (Merkle Tree)}

\noindent \includegraphics[width=12.3cm]{images/merkle-tree.png}

\vspace{0.50cm}
\noindent Given a set of data blocks in the lowest level of the tree, each data block has a hash pointer to the specific data blocks. Each group of hash pointers then has another hash pointer which points to that group of hash pointers. This structure continues upwards all up to the root of the tree, which is being remembered and is "impossible" to tamper with.\\

\noindent The benefits of using a merkle tree instead of a series of all (or most of) the data blocks and hash pointers, is that one only has to show the specific path in the tree to the data block. This saves a lot of computations, with O-notation: $ O(log\ n) $ items.\\

\noindent The advantages are therefore that a tree can hold many items, but just need to remember the root hash in order to verify the contents. The membership verification can be computed in $ O(log\ n) $ time/space. There is also a variant of sorted Merkle trees that can verify non-membership in $ O(log\ n) $, by detecting whether a node should be between two tree nodes or not.\\

\noindent More generally, it is possible to use hash pointers in any pointer-based data structure that has no cycles.


\subsection{Digital Signatures}

\noindent What we want from signatures is that only 'you' can sign, while anyone can verify. A signature is tied to a particular document and cannot be cut-and-pasted to another document.

\noindent Some practical requirements of a digital signature includes:

\begin{itemize}
    \item Algorithms are randomized, which means a good sources of randomness is needed
    \item There is a limit on the message size, which can easily be fixed by using the hashed message, rather than the message itself (fixed 256 bits)
    \item A fun trick is to sign a hash pointer, so that a signature "covers"/"protects" the whole structure.
\end{itemize}

\noindent Bitcoin uses ECDSA (Elliptic Curve Digital Signature Algorithm) as a standard signature algorithm. One essential requirement of this algorithm is good randomness. Without good randomness, the private key ($ pk $) has probably been leaked.


\subsection{Public Keys as Identifiers}

\noindent A useful trick is to treat public key as an identity. If you have a valid verification of a signature, it is possible to know that the holder of the public key is saying the "message". In order to "speak for" a public key, you must know know the matching secret key (private key).\\

\noindent It is possible to create a new identity by creating a random key-pair ($ sk $, $ pk $), where the public key ($ pk $) is the public "name" and the secret key ($ sk $) makes it possible to "speak for" that identity. You therefore control the identity, because only you know the secret key if the public key "looks random", nobody needs to know who you are.\\

\noindent Decentralized identity management makes it possible for anyone to make a new identity at any time, or the possiblity to make as many as you want. There is no central point of coordination and \underline{these identities are called "addresses" in Bitcoin}.\\

\noindent Addresses are not directly connected to real-world identity, but by observing the address' activity over time it is possible to make inferences and links to who might have the identity of a specific address.


\subsection{A Simple Cryptocurrency (GoofyCoin)}

\noindent The rules of GoofyCoin are:

\begin{enumerate}
    \item Goofy can create new coins by signing a statement that he creates a new coin with a unique coin id.
    \item Whoever owns a coin can pass it on to someone else by signing a statement saying sign on this coin to person $ x $.
    \item It is possible to verify the validity of a coin by following the chain of transactions and verifying all the signatures along the way.
\end{enumerate}

\subsubsection{Double-Spending Attack}

\noindent A security flaw in GoofyCoin is that it is possible to spend the same coin multiple times, which is also the main security challenge in cryptocurrencies.\\

\noindent Another cryptocurrency which does solve this problem is ScroogeCoin. In short, ScroogeCoin publishes a history of all transactions by adding an id to each transaction block (a blockchain, signed by Scrooge). It is then not possible to spend the same coin multiple times, since the id detects that the coin transaction is used before.

\end{document}
