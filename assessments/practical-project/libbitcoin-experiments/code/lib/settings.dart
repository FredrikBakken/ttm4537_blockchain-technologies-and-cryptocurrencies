library Libbitcoin_Experiments.settings;

import 'package:Libbitcoin_Experiments/classes.dart' as c;

// Payment methods
bool basic_payment    = false;
bool op_return        = false;
bool multisignatures  = false;

// Current actors
c.Wallet wallet_1;
c.Wallet wallet_2;

// Payment amounts
int paying_amount     = 40000;
int paying_fee        = 10000;

// Payment percents (Total must be <=100%, remaining returned to sender)
int percent_paying_amount = 30;
int percent_paying_fee    = 10;

// Other global variable settings
bool testnet          = true;
bool percent_paying   = true;
String group_number   = '11';
