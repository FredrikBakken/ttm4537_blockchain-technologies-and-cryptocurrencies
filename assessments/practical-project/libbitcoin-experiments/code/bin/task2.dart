import 'package:Libbitcoin_Experiments/base.dart' as b;
import 'package:Libbitcoin_Experiments/settings.dart' as s;
import 'package:Libbitcoin_Experiments/sendBTC.dart' as sBTC;


void execute_task2() async {
  // Initialize variables
  int amount = 90;
  int fee = 10;
  bool percent_paying = true;

  // Encode string value to Base16
  String message = "TTM4537: Group 17";
  String encoded_message = await b.execute_process(['base16-encode', message]);
  
  // Script encode the encoded string
  String encoded_script = await b.execute_process(['script-encode', 'return [' + encoded_message + ']']);

  // Printing to console
  print('\n\nTask 2: Posting data using OP RETURN');
  print('Message to encode: ' + message);
  print('Encoded message:   ' + encoded_message);
  print('Encoded script:    ' + encoded_script);

  // Send OP RETURN transaction
  await sBTC.fetchHistory(s.wallet_1);
  await sBTC.sendBTC(s.wallet_1, s.wallet_1, amount, fee, percent_paying, execute_tx: false, basic_payment: false, op_return: true, multisignatures: false, ntxs: 1, encoded_script: encoded_script);
}
