
# Class: Transaction
class Transaction(object):
    """ Class for representing a Bitcoin transaction
    """

    # Class initializer
    def __init__(self, block_hash, block_height, block_time,
                 confirmations, created_at, fee, t_hash,
                 inputs_count, inputs_value, is_coinbase,
                 is_double_spend, is_sw_tx, lock_time,
                 outputs_count, outputs_value, sigops, size,
                 version, vsize, weight, witness_hash,
                 inputs=[], outputs=[]):
        
        self.block_hash                 = block_hash
        self.block_height               = block_height
        self.block_time                 = block_time
        self.confirmations              = confirmations
        self.created_at                 = created_at
        self.fee                        = fee
        self.hash                       = t_hash
        self.inputs_count               = inputs_count
        self.inputs_value               = inputs_value
        self.is_coinbase                = is_coinbase
        self.is_double_spend            = is_double_spend
        self.is_sw_tx                   = is_sw_tx
        self.lock_time                  = lock_time
        self.outputs_count              = outputs_count
        self.outputs_value              = outputs_value
        self.sigops                     = sigops
        self.size                       = size
        self.version                    = version
        self.vsize                      = vsize
        self.weight                     = weight
        self.witness_hash               = witness_hash
        self.inputs                     = inputs
        self.outputs                    = outputs


class Transaction_Input(object):
    """ Class for representing a Bitcoin transaction's inputs
    """

    # Class initializer
    def __init__(self, prev_addresses, prev_position,
                 prev_tx_hash, prev_type, prev_value, sequence,
                 script_asm=None, script_hex=None, witness=None):

        self.prev_addresses  = prev_addresses
        self.prev_position   = prev_position
        self.prev_tx_hash    = prev_tx_hash
        self.prev_type       = prev_type
        self.prev_value      = prev_value
        self.sequence        = sequence
        self.script_asm      = script_asm
        self.script_hex      = script_hex
        self.witness         = witness


class Transaction_Output(object):
    """ Class for representing a Bitcoin transaction's outputs
    """

    # Class initializer
    def __init__(self, addresses, spent_by_tx, spent_by_tx_position,
                 out_type, value, script_asm=None, script_hex=None):

        self.addresses              = addresses
        self.spent_by_tx            = spent_by_tx
        self.spent_by_tx_position   = spent_by_tx_position
        self.out_type               = out_type
        self.value                  = value
        self.script_asm             = script_asm
        self.script_hex             = script_hex



def new_transaction(data):
    # Initialize inputs list
    inputs = []

    # Initialize outputs list
    outputs = []

    # Loop through inputs
    for i in range(0, len(data['inputs'])):
        # KeyError exception for optional parameters
        try:
            in_script_asm  = data['inputs'][i]['script_asm']
            in_script_hex  = data['inputs'][i]['script_hex']
            in_witness     = data['inputs'][i]['witness']
        except KeyError:
            in_script_asm  = None
            in_script_hex  = None
            in_witness     = None

        # Create Transaction_Input object
        transaction_input = Transaction_Input(
            prev_addresses  = data['inputs'][i]['prev_addresses'],
            prev_position   = data['inputs'][i]['prev_position'],
            prev_tx_hash    = data['inputs'][i]['prev_tx_hash'],
            prev_type       = data['inputs'][i]['prev_type'],
            prev_value      = data['inputs'][i]['prev_value'],
            sequence        = data['inputs'][i]['sequence'],
            script_asm      = in_script_asm,
            script_hex      = in_script_hex,
            witness         = in_witness
        )

        # Append transaction inputs to list
        inputs.append(transaction_input)
    

    # Loop through outputs
    for i in range(0, len(data['outputs'])):
        # KeyError exception for optional parameters
        try:
            out_script_asm  = data['outputs'][i]['script_asm']
            out_script_hex  = data['outputs'][i]['script_hex']
        except KeyError:
            out_script_asm  = None
            out_script_hex  = None
        
        # Create Transaction_Output object
        transaction_output = Transaction_Output(
            addresses              = data['outputs'][i]['addresses'],
            spent_by_tx            = data['outputs'][i]['spent_by_tx'],
            spent_by_tx_position   = data['outputs'][i]['spent_by_tx_position'],
            out_type               = data['outputs'][i]['type'],
            value                  = data['outputs'][i]['value'],
            script_asm             = out_script_asm,
            script_hex             = out_script_hex
        )

        # Append transaction outputs to list
        outputs.append(transaction_output)


    # Define a new Bitcoin transaction
    transaction = Transaction(
        block_hash                 = data['block_hash'],
        block_height               = data['block_height'],
        block_time                 = data['block_time'],
        confirmations              = data['confirmations'],
        created_at                 = data['created_at'],
        fee                        = data['fee'],
        t_hash                     = data['hash'],
        inputs_count               = data['inputs_count'],
        inputs_value               = data['inputs_value'],
        is_coinbase                = data['is_coinbase'],
        is_double_spend            = data['is_double_spend'],
        is_sw_tx                   = data['is_sw_tx'],
        lock_time                  = data['lock_time'],
        outputs_count              = data['outputs_count'],
        outputs_value              = data['outputs_value'],
        sigops                     = data['sigops'],
        size                       = data['size'],
        version                    = data['version'],
        vsize                      = data['vsize'],
        weight                     = data['weight'],
        witness_hash               = data['witness_hash'],
        inputs                     = inputs,
        outputs                    = outputs
    )

    # Return the Bitcoin transaction
    return transaction
