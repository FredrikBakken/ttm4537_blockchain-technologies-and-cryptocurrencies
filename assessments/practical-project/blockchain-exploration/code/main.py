# http://btc.com/api-doc


# Imports
import json
import time
import math
import requests

from forex_python.bitcoin import BtcConverter
from datetime import date, datetime, timedelta

from response_types.block import new_block
from response_types.transaction import new_transaction


##### START: BTC API
class BTC_API(object):
    """ Designed to simplify the GET/POST requests
    towards the BTC API.
    """

    # Class initializer
    def __init__(self):
        self.url = 'https://chain.api.btc.com/v3/'

    
    """ Internal methods
    """

    def _get_response(self, url):
        """ Return contents in JSON-format.
        Open a request to the website and fetch the
        text, then load the text as json-format.
        """

        # Fetch web text and load as json
        response = requests.get(url).text
        json_data = json.loads(response)

        # Return results
        return json_data


    """ Callable methods
    """

    def get_block_info(self, blocks=[None]):
        url = self.url + 'block/'
        
        for block in blocks:
            url += str(block) + ','
        
        return self._get_response(url[:-1])
    

    def get_block_list(self, blocks_date=None):
        url = self.url + 'block/date/'

        if isinstance(blocks_date, date):
            blocks_date = blocks_date.strftime('%Y%m%d')
        
        url += blocks_date

        return self._get_response(url)
    

    def get_block_transaction(self, block=None, page=1):
        url = self.url + 'block/' + str(block) + '/tx?page=' + str(page)

        return self._get_response(url)
##### END: BTC API



def question_one():
    """ Find a block from 2018 with block height which ends with the same 3 digits as
    your student number ends with, and has at least 10 transactions. This defines your
    target block and target day. Some of the following questions involve these targets
    and some do not.
    """

    # Initialize the Bitcoin API
    btc = BTC_API()

    # Define the student ID
    student_id = '772515'

    # Getting the from an to dates dynamically
    from_date   = datetime(2018, 1, 1)
    to_date     = datetime.now()

    # Get the first and latest block in 2018
    first_block     = btc.get_block_list(blocks_date=from_date)['data'][-1]
    latest_block    = btc.get_block_list(blocks_date=to_date)['data'][0]

    # Turn the blocks into Block objects
    start_block = new_block(first_block)
    end_block   = new_block(latest_block)

    # Student ID ending (last three digits)
    id_ending = student_id[-3:]

    # Placeholder for the initial block height
    initial_block = start_block.height

    # Check if the three last digits of the starting block
    # is higher than the three last digits of the student id
    if (int(str(start_block.height)[-3:]) > int(id_ending)):
        initial_block = start_block.height + 1000
    
    # Update the initial block with the student ID ending
    initial_block = int(str(initial_block)[:-3] + id_ending)

    # Loop from the initial block upto the end block of 2018
    for height in range(initial_block, end_block.height, 1000):
        # Necessary in order to avoid 403 Forbidden issues
        time.sleep(1)
        
        # Get block transactions
        block_transactions = btc.get_block_transaction(block=height)

        # Find the total transactions for a specific block
        number_of_transactions = int(block_transactions['data']['total_count'])

        # Check if target block and day has been found
        if number_of_transactions > 10:
            # Collect the current block's information
            current_block = btc.get_block_info(blocks=[height])['data']

            # Define the target block/day according to specifications
            target_block    = new_block(current_block)
            target_day      = target_block.created_at

            ##### START: Print the results of question 1
            print('\n\n-------------------------------------------------------------')
            print('-------------------------------------------------------------')
            print('QUESTION 1 | Find the target block and target day')
            print('-------------------------------------------------------------')
            print('Student ID: {}'.format(student_id))
            print('First block created in 2018:  {}'.format(start_block.height))
            print('Latest block created in 2018: {}'.format(end_block.height))
            print('Initial block with student ID ending: {}\n'.format(initial_block))
            print('RESULTS:')
            print('Target block found with more than 10 transactions: {}'.format(target_block.height))
            print('Number of transactions in target block: {}'.format(target_block.size))
            print('Timestamp of block creation: {} ({})'.format(target_day, datetime.fromtimestamp(target_day)))
            print('-------------------------------------------------------------\n')
            ##### END: Print the results of question 1

            # Necessary in order to avoid 403 Forbidden issues
            time.sleep(1)

            # Return the results
            return target_block, target_day


def question_two():
    """ What is the first block in the whole of the Bitcoin blockchain with a normal transaction
    transferring value from one address to another? (This excludes the coinbase
    transaction which is the first transaction in every block.)
    """

    # Initialize the Bitcoin API
    btc = BTC_API()

    # Initialize the block multiplier
    height_multiplier = 0

    # Variable to define if normal transaction found
    normal_transaction_found = False

    # Iterate upwards until the first transaction is found
    while not normal_transaction_found:
        # Necessary in order to avoid 403 Forbidden issues
        time.sleep(1)

        # Define the current set of blocks
        heights = [(x + height_multiplier) for x in range(10)]

        # Get all current blocks
        blocks = btc.get_block_info(blocks=heights)['data']

        # Loop through all current blocks
        for current_block in blocks:
            # Get the number of transactions for the current block
            number_of_transactions = current_block['tx_count']

            # Create a Block object of the current block
            if number_of_transactions > 1:
                # Placeholder to keep all transactions found
                list_of_transactions = []

                # Create a Block object if there are more than 1 transaction
                result_block = new_block(current_block)

                # Necessary in order to avoid 403 Forbidden issues
                time.sleep(1)

                # Fetch all transactions in current block
                transactions = btc.get_block_transaction(block=result_block.height)['data']['list']

                # Append all found Transaction objects
                for transaction in transactions:
                    list_of_transactions.append(new_transaction(transaction))

                ##### START: Print the results of question 2
                print('\n\n-------------------------------------------------------------')
                print('-------------------------------------------------------------')
                print('QUESTION 2 | Find the first block with a normal transaction')
                print('-------------------------------------------------------------')
                print('First block with normal transaction:   {}'.format(result_block.height))
                print('Number of transactions in block found: {}'.format(result_block.tx_count))
                print('-------------------------------------------------------------')

                # Loop through the transactions
                for i in range(0, len(list_of_transactions)):
                    print('\nTRANSACTION {}'.format(i+1))
                    print('Block height   | {}'.format(list_of_transactions[i].block_height))
                    print('Block hash     | {}'.format(list_of_transactions[i].block_hash))
                    print('Hash           | {}'.format(list_of_transactions[i].hash))
                    print('Inputs value   | {}'.format(list_of_transactions[i].inputs_value))
                    print('Is coinbase    | {}'.format(list_of_transactions[i].is_coinbase))
                    print('Inputs count   | {}'.format(list_of_transactions[i].inputs_count))
                    print('Outputs count  | {}'.format(list_of_transactions[i].outputs_count))

                    # Loop through the current transaction inputs
                    for j in range(0, len(list_of_transactions[i].inputs)):
                        print('\nTRANSACTION {} | INPUT {}'.format(i+1, j+1))
                        print('Previous addresses         | {}'.format(list_of_transactions[i].inputs[j].prev_addresses))
                        print('Previous position          | {}'.format(list_of_transactions[i].inputs[j].prev_position))
                        print('Previous transaction hash  | {}'.format(list_of_transactions[i].inputs[j].prev_tx_hash))
                        print('Previous type              | {}'.format(list_of_transactions[i].inputs[j].prev_type))
                        print('Previous value             | {}'.format(list_of_transactions[i].inputs[j].prev_value))
                        print('Sequence                   | {}'.format(list_of_transactions[i].inputs[j].sequence))
                        
                    # Loop through the current transaction outputs
                    for j in range(0, len(list_of_transactions[i].outputs)):
                        print('\nTRANSACTION {} | OUTPUT {}'.format(i+1, j+1))
                        print('Address                        | {}'.format(list_of_transactions[i].outputs[j].addresses))
                        print('Value                          | {}'.format(list_of_transactions[i].outputs[j].value))
                        print('Type                           | {}'.format(list_of_transactions[i].outputs[j].out_type))
                        print('Spent by transaction           | {}'.format(list_of_transactions[i].outputs[j].spent_by_tx))
                        print('Spent by transaction position  | {}'.format(list_of_transactions[i].outputs[j].spent_by_tx_position))
                    
                    print('-------------------------------------------------------------\n')
                ##### END: Print the results of question 2

                # Necessary in order to avoid 403 Forbidden issues
                time.sleep(1)

                # Return the results
                return result_block, list_of_transactions

        # Update the block number
        height_multiplier += 10


def question_three(target_day):
    """ How many Bitcoins have been issued before your target day?
    """

    # Initialize the Bitcoin API
    btc = BTC_API()

    # Initialize variables
    block_reward = 50
    reward_halves = 210000

    # Convert target day timestamp to year-month-day format
    target_day_date = datetime.fromtimestamp(target_day)

    # Use timedelta to get the day before the target day
    day_before = (target_day_date - timedelta(days=1)).strftime('%Y%m%d')

    # Get the last block of the day before the target day
    last_block = btc.get_block_list(blocks_date=day_before)['data'][0]

    # Turn the last block before target day into a Block object
    block = new_block(last_block)

    # Get the block's height
    height = block.height

    # Total number of issued Bitcoins
    issued = 0
    
    # Calculate the number of reward halves that has occured
    number_of_halves = height / reward_halves

    # If no reward halves has occured
    if number_of_halves < 1:
        issued = block_reward * height
    
    # Otherwise
    else:
        # Variable to store the reward after halves
        reward = block_reward

        # Loop through each reward halves and calculate the issued total
        for i in range(int(number_of_halves)):
            issued += (block_reward/(i + 1) * reward_halves)
            reward = reward / 2

        # Remaining number of blocks
        remaining = height - (reward_halves * int(number_of_halves))

        # Add the final remaining reward values
        issued += (reward * remaining)

    ##### START: Print the results of question 3
    print('\n\n-------------------------------------------------------------')
    print('-------------------------------------------------------------')
    print('QUESTION 3 | Find the total number of issued Bitcoins')
    print('-------------------------------------------------------------')
    print('Last height of the day before: {}'.format(height))
    print('Total number of coins issued: {}'.format(int(issued)))
    print('-------------------------------------------------------------\n')
    ##### END: Print the results of question 3

    # Return the results
    return issued


def question_four():
    """ On what date did the block reward change to 12.5 Bitcoins?
    Was there any noticeable effect on the hash rate?
    """

    # Initialize the Bitcoin API
    btc = BTC_API()

    # Fetch the block information for block 420000
    block_info = btc.get_block_info(blocks=[420000])['data']

    # Turn block information into a Block object
    block = new_block(block_info)

    # If the reward of the block is equal to 12.5 Bitcoins
    if (block.reward_block == 1250000000):
        ##### START: Print the results of question 4
        print('\n\n-------------------------------------------------------------')
        print('-------------------------------------------------------------')
        print('QUESTION 4 | Change in rewards to 12.5 Bitcoins/block')
        print('-------------------------------------------------------------')
        print('Reward change time: {} ({})'.format(datetime.fromtimestamp(block.timestamp).strftime('%Y-%m-%d %H:%M:%S'), block.timestamp))
        print('-------------------------------------------------------------\n')
        ##### END: Print the results of question 4

        # Return the results
        return datetime.fromtimestamp(block.timestamp)


def question_five(target_block):
    """ What was the dollar value of the block reward for your target block on the day it
    was earned?
    """

    # Initialize the Forex Bitcoin - USD converter
    bitcoinUSD = BtcConverter()

    # Fetch the day of the target block
    day_of_target_block = datetime.fromtimestamp(target_block.timestamp)

    # Rewards of target block
    rewards = (target_block.reward_block * 10**(-8))

    # Fetch the Bitcoin value on the exact time of the target block
    price = bitcoinUSD.get_previous_price('USD', day_of_target_block)

    ##### START: Print the results of question 5
    print('\n\n-------------------------------------------------------------')
    print('-------------------------------------------------------------')
    print('QUESTION 5 | Dollar value of the block reward')
    print('-------------------------------------------------------------')
    print('Day and time of the target block:  {}'.format(day_of_target_block))
    print('Price per. Bitcoin in USD at time: ${}'.format(price))
    print('Total value of reward block (USD): ${} * {} = ${}'.format(price, rewards, (rewards*price)))
    print('-------------------------------------------------------------\n')
    ##### END: Print the results of question 5

    # Return the results
    return price


def question_six(target_block):
    """ Find the latest (newest) block before your target block where difficulty decreased
    compared to the previous one.
    """

    # Initialize the Bitcoin API
    btc = BTC_API()

    # Initialize current block
    current_block = None

    # Initialize the current day
    current_day = datetime.fromtimestamp(target_block.timestamp)

    # Loop forever until decreased difficulty found
    while True:
        # Necessary in order to avoid 403 Forbidden issues
        time.sleep(1)

        # Fetch data for the current day
        current_day_data = btc.get_block_list(blocks_date=current_day.strftime('%Y%m%d'))['data']

        # Initialize a placeholder for Block objects
        current_day_blocks = []

        # Append all current day blocks to list
        for block in current_day_data:
            # Create a Block object
            block_object = new_block(block)

            # Check that Block object is before target block
            if block_object.height < target_block.height:
                # Initialize current block
                if current_block == None:
                    current_block = block_object

                # Append Block to list
                current_day_blocks.append(block_object)
        
        # Sort all the blocks for the current day
        current_day_blocks_sorted = sorted(current_day_blocks, key=lambda x : x.height, reverse=True)

        # Go through the sorted blocks
        for i in range(0, len(current_day_blocks_sorted) - 1):
            previous_block = current_day_blocks_sorted[i+1]

            # Check if the newer (higher height) block has decreased difficulty
            if current_block.difficulty < previous_block.difficulty:
                ##### START: Print the results of question 6
                print('\n\n-------------------------------------------------------------')
                print('-------------------------------------------------------------')
                print('QUESTION 6 | Block with decreased difficulty compared to previous')
                print('-------------------------------------------------------------')
                print('Decreased block height:     {}'.format(current_block.height))
                print('Decreased block difficulty: {}'.format(current_block.difficulty))
                print('Decreased block date/time:  {}\n'.format(datetime.fromtimestamp(current_block.timestamp).strftime('%Y-%m-%d %H:%M:%S')))
                print('Previous block height:      {}'.format(previous_block.height))
                print('Previous block difficulty:  {}'.format(previous_block.difficulty))
                print('Previous block date/time:   {}'.format(datetime.fromtimestamp(previous_block.timestamp).strftime('%Y-%m-%d %H:%M:%S')))
                print('-------------------------------------------------------------\n')
                ##### END: Print the results of question 6

                # Return the results
                return current_block, previous_block
            
            # Update current block variable
            current_block = current_day_blocks_sorted[i + 1]

        # Update current day
        current_day = current_day - timedelta(days=1)


def question_seven(target_block):

    def part_one(target_block):
        """ Find the inter-block times for the 20 blocks before and the 20 blocks after your
        target block.
        """

        # Initialize the inter-block times
        times_before = []
        times_after  = []

        # Initialize the Bitcoin API
        btc = BTC_API()

        # Get the target block's height
        target_block_height = target_block.height

        # Initialize the 20 block heights before target block
        heights_before = [(target_block_height - x - 1) for x in range(20)]

        # Initialize the 20 block height after target block
        heights_after = [(target_block_height + x + 1) for x in range(20)]

        # Fetch the data for the blocks before the target block
        blocks_before = btc.get_block_info(blocks=heights_before)['data']

        # Loop through the blocks before the target block
        for i in range(0, len(blocks_before) - 1):
            # Create Block objects
            current = new_block(blocks_before[i])
            future  = new_block(blocks_before[i + 1])

            # Fetch the timestamps for from the Blocks
            current_block   = datetime.fromtimestamp(current.timestamp)
            future_block    = datetime.fromtimestamp(future.timestamp)

            # Calculate the inter-block time
            time_difference = int((current_block - future_block).total_seconds())

            # Append inter-block time to list
            times_before.append(time_difference)
        
        # Necessary in order to avoid 403 Forbidden issues
        time.sleep(1)

        # Fetch the data for the blocks after the target block
        blocks_after = btc.get_block_info(blocks=heights_after)['data']

        # Loop through the blocks after the target block
        for i in range(0, len(blocks_after) - 1):
            # Create Block objects
            current = new_block(blocks_after[i])
            future  = new_block(blocks_after[i + 1])

            # Fetch the timestamps for from the Blocks
            current_block   = datetime.fromtimestamp(current.timestamp)
            future_block    = datetime.fromtimestamp(future.timestamp)

            # Calculate the inter-block time
            time_difference = int((future_block - current_block).total_seconds())

            # Append inter-block time to list
            times_after.append(time_difference)
        
        # Return the inter-block times before and after
        return times_before, times_after

    
    def part_two(times_before, times_after):
        """ What is the mean and variance of these times?
        """

        def calculate_mean(inter_times):
            total = 0

            for inter_time in inter_times:
                total += inter_time

            mean = total / len(inter_times)

            return mean
        

        def calculate_variance(inter_times, mean):
            total = 0

            for inter_time in inter_times:
                total += (inter_time - mean)**2
            
            variance = math.sqrt(total / (len(inter_times) - 1))

            return variance

        # Calculate the mean values
        mean_before = calculate_mean(times_before)
        mean_after = calculate_mean(times_after)

        # Calculate the variance values
        variance_before = calculate_variance(times_before, mean_before)
        variance_after = calculate_variance(times_after, mean_after)

        ##### START: Print the results of question 7
        print('\n\n-------------------------------------------------------------')
        print('-------------------------------------------------------------')
        print('QUESTION 7 | Mean and variances for inter-block times')
        print('-------------------------------------------------------------')
        print('Inter-block times before target block:')
        print('Inter-block times: {}'.format(times_before))
        print('Mean: {}'.format(mean_before))
        print('Variance: {}\n'.format(variance_before))

        print('Inter-block times after target block:')
        print('Inter-block times: {}'.format(times_after))
        print('Mean: {}'.format(mean_after))
        print('Variance: {}'.format(variance_after))
        print('-------------------------------------------------------------\n')
        ##### END: Print the results of question 7

        # Return the results
        return [times_before, mean_before, variance_before], [times_after, mean_after, variance_after]

    # Execute the first part of the question
    times_before, times_after = part_one(target_block)

    # Execute the second part of the question
    before, after = part_two(times_before, times_after)

    # Return the results
    return before, after


def question_eight(target_day):
    """ What are the longest and the shortest inter-block times in the four weeks before
    your target day?
    """

    # Initialize the Bitcoin API
    btc = BTC_API()

    # Initialize the number of days of days before the target day
    days = 28

    # Initialize variables to find
    longest  = None
    shortest = None

    # Initialize the list to hold the longest and shortest blocks
    longest_blocks  = []
    shortest_blocks = []

    # Initialize the current day variable
    current_day = datetime.fromtimestamp(target_day)

    # Initialize placeholder for current block
    current_block = None

    # Loop through all 28 days (4 weeks)
    for _ in range(days):
        # Update current day variable
        current_day = current_day - timedelta(days=1)

        # Necessary in order to avoid 403 Forbidden issues
        time.sleep(1)

        # Fetch data for the current day
        current_day_data = btc.get_block_list(blocks_date=current_day.strftime('%Y%m%d'))['data']

        # Initialize a placeholder for Block objects
        current_day_blocks = []

        # Append all current day blocks to list
        for block in current_day_data:
            # Create a Block object
            block_object = new_block(block)

            # Append Block to list
            current_day_blocks.append(block_object)
        
        # Sort all the blocks for the current day
        current_day_blocks_sorted = sorted(current_day_blocks, key=lambda x : x.height, reverse=True)

        # Initialize current block with Block object
        if current_block == None:
            current_block = current_day_blocks_sorted[0]

        for i in range(0, len(current_day_blocks_sorted) - 1):
            # Define the next block
            next_block = current_day_blocks_sorted[i + 1]

            # Get the timestamps of the current and next blocks
            current_block_time  = datetime.fromtimestamp(current_block.timestamp)
            next_block_time     = datetime.fromtimestamp(next_block.timestamp)

            # Find the time difference between the blocks
            time_difference = abs(int((current_block_time - next_block_time).total_seconds()))

            # Initialize the longest and shortest variables with values
            if longest == None and shortest == None:
                longest = time_difference
                shortest = time_difference
                longest_blocks = [current_block, next_block]
                shortest_blocks = [current_block, next_block]
            
            # Check if current inter-block time is longer than longest
            if time_difference > longest:
                longest = time_difference
                longest_blocks = [current_block, next_block]
            
            # Check if current inter-block time is shorter than shortest
            if time_difference < shortest:
                shortest = time_difference
                shortest_blocks = [current_block, next_block]

            # Update current block
            current_block = next_block
    
    ##### START: Print the results of question 8
    print('\n\n-------------------------------------------------------------')
    print('-------------------------------------------------------------')
    print('QUESTION 8 | Longest and shortest inter-block times')
    print('-------------------------------------------------------------')
    print('Target day: {}'.format(datetime.fromtimestamp(target_day).strftime('%Y-%m-%d')))
    print('Start date: {}'.format((datetime.fromtimestamp(target_day) - timedelta(days=28)).strftime('%Y-%m-%d')))
    print('End date:   {}\n'.format((datetime.fromtimestamp(target_day) - timedelta(days=1)).strftime('%Y-%m-%d')))
    print('Longest inter-block times:')
    print('Time: {} seconds between block {} and block {}\n'.format(longest, longest_blocks[0].height, longest_blocks[1].height))
    print('Shortest inter-block times:')
    print('Time: {} seconds between block {} and block {}'.format(shortest, shortest_blocks[0].height, shortest_blocks[1].height))
    print('-------------------------------------------------------------\n')
    ##### END: Print the results of question 8

    # Return the results
    return longest, longest_blocks, shortest, shortest_blocks


def question_nine(target_block):

    def part_one(target_block):
        """ What was the maximum and minimum transaction fee paid in your target block?
        """

        # Initialize the Bitcoin API
        btc = BTC_API()

        # Initial variables to store maximum and minimum transaction fees
        maximum_fee = None
        minimum_fee = None

        # Fetch the target block's transactions
        block_transactions = btc.get_block_transaction(block=target_block.height)['data']

        # Total number of transactions in target block
        total_count = block_transactions['total_count']
        
        # Total number of pages with transactions
        pages = math.ceil(total_count / 50)

        # Loop through each page of target block's transactions
        for page in range(1, pages+1):
            # Get all transactions on the current page
            current_page_transactions = block_transactions['list']

            # Loop through all transactions on the current page
            for transaction in current_page_transactions:
                transaction_object = new_transaction(transaction)

                # Initialize maximum and minimum fee
                if maximum_fee == None and minimum_fee == None:
                    maximum_fee = transaction_object
                    minimum_fee = transaction_object
                
                # Update maximum fee if a new maximum is found
                if transaction_object.fee > maximum_fee.fee:
                    maximum_fee = transaction_object
                
                # Update minimum fee if a new minimum is found
                if transaction_object.fee < minimum_fee.fee:
                    minimum_fee = transaction_object

            # Necessary in order to avoid 403 Forbidden issues
            time.sleep(1)

            # Pull data for the next page of transactions
            block_transactions = btc.get_block_transaction(block=target_block.height, page=page+1)['data']

            # Print status information
            # print('Page {} done!'.format(page))

        # Return the results
        return maximum_fee, minimum_fee


    def part_two():
        """ What was the largest transaction fee paid since 1 January 2018, measured in Bitcoins?
        """

        # Initialize the Bitcoin API
        btc = BTC_API()

        # Necessary in order to avoid 403 Forbidden issues
        time.sleep(1)

        # Initialize the start date with January 1st 2018
        start_of_year = datetime(2018, 1, 1)

        # Fetch first block of 2018
        first_block = new_block(btc.get_block_list(blocks_date=start_of_year.strftime('%Y%m%d'))['data'][-1])

        # Request ordered blocks by Bitcoin fees from 2018 (Blockchair)
        response = requests.get('https://api.blockchair.com/bitcoin/blocks?s=fee_total(desc)&q=id(' + str(first_block.height) + '..)').json()

        # Block with highest fees in 2018
        highest_fees = response['data'][0]

        # Return the results
        return [highest_fees['id'], highest_fees['time'], highest_fees['fee_total']]


    # Execute the first part of the question
    maximum_fee, minimum_fee = part_one(target_block)

    # Execute the second part of the question
    largest_transaction_fee = part_two()

    ##### START: Print the results of question 9
    print('\n\n-------------------------------------------------------------')
    print('-------------------------------------------------------------')
    print('QUESTION 9 | Transaction fees in target block and 2018')
    print('-------------------------------------------------------------')
    print('Part 1 - Maximum and minimum transaction fee in target block:')
    print('Maximum fee: {} Bitcoins ({} Satoshis)'.format(maximum_fee.fee * 10**(-8), maximum_fee.fee))
    print('Minimum fee: {} Bitcoins ({} Satoshis)\n'.format(minimum_fee.fee * 10**(-8), minimum_fee.fee))

    print('Part 2 - Largest transaction fee paid since January 1st 2018:')
    print('Block height: {}'.format(largest_transaction_fee[0]))
    print('Block date/time: {}'.format(largest_transaction_fee[1]))
    print('Block fees: {} Bitcoins ({} Satoshis)'.format(largest_transaction_fee[2] * 10**(-8), largest_transaction_fee[2]))
    print('-------------------------------------------------------------\n')
    ##### END: Print the results of question 9

    # Return the results
    return maximum_fee, minimum_fee, largest_transaction_fee


def question_ten(target_day):

    def find_difficulty_changes(target_day, number_of_changes=1):
        # Initialize the Bitcoin API
        btc = BTC_API()

        # Initialize a list to hold all blocks found
        result_blocks = []

        # Initialize the current day variable
        current_day = datetime.fromtimestamp(target_day)

        # Initialize placeholder for current block
        current_block = None

        # Loop until difficulty change found
        while True:
            # Update current day variable
            current_day = current_day - timedelta(days=1)

            # Necessary in order to avoid 403 Forbidden issues
            time.sleep(1)

            # Fetch data for the current day
            current_day_data = btc.get_block_list(blocks_date=current_day.strftime('%Y%m%d'))['data']

            # Initialize a placeholder for Block objects
            current_day_blocks = []

            # Append all current day blocks to list
            for block in current_day_data:
                # Create a Block object
                block_object = new_block(block)

                # Append Block to list
                current_day_blocks.append(block_object)
            
            # Sort all the blocks for the current day
            current_day_blocks_sorted = sorted(current_day_blocks, key=lambda x : x.height, reverse=True)

            # Initialize current block with Block object
            if current_block == None:
                current_block = current_day_blocks_sorted[0]

            for i in range(0, len(current_day_blocks_sorted) - 1):
                # Check if necessary number of blocks has been found
                if len(result_blocks) == number_of_changes:
                    return result_blocks

                # Define the next block
                next_block = current_day_blocks_sorted[i + 1]

                # Check if there is a difficulty change
                if current_block.difficulty != next_block.difficulty:
                    # Append new block found to result list
                    result_blocks.append(current_block)
                
                # Update current block
                current_block = next_block

            # Status message
            # print('{} | {} | {}'.format(current_day.strftime('%Y-%m-%d'), len(result_blocks), current_block.difficulty))


    def find_average_time_difference(blocks):
        # Average time between difficulty changes
        avg_seconds = abs(int((datetime.fromtimestamp(blocks[0].timestamp) - datetime.fromtimestamp(blocks[-1].timestamp)).total_seconds()) / len(blocks))

        # Find the average days
        days = str(int(avg_seconds // (24 * 3600)))
        avg_seconds %= (24 * 3600)

        # Find the average hours
        hours = str(int(avg_seconds // 3600))
        avg_seconds %= 3600

        # Find the average mintes
        minutes = str(int(avg_seconds // 60))
        avg_seconds %= 60

        # Find the average seconds
        seconds = str(int(avg_seconds))

        # Return the found average time difference
        return ('{} days, {} hours, {} minutes, and {} seconds'.format(days, hours, minutes, seconds))


    def part_one(target_day):
        """ When was the last change in difficulty before your target day?
        """

        # Return the results found
        return find_difficulty_changes(target_day)[0]


    def part_two(target_day):
        """ Find the previous 10 changes in difficulty and the time between them. What
        is average time between difficulty changes?
        """

        # Find the 10 previous difficulty changes
        blocks = find_difficulty_changes(target_day, number_of_changes=10)

        # Find the average time differences between difficulty changes
        avg_time_difference = find_average_time_difference(blocks)

        # Return the results found
        return blocks, avg_time_difference
    

    # Execute the first part of the question
    results_part_one = part_one(target_day)

    # Execute the second part of the question
    results_part_two, avg_time_difference = part_two(target_day)

    ##### START: Print the results of question 10
    print('\n\n-------------------------------------------------------------')
    print('-------------------------------------------------------------')
    print('QUESTION 10 | Difficulty changes (times)')
    print('-------------------------------------------------------------')
    print('Part 1 - Last change in difficulty before target day: {} (block {})\n'.format(datetime.fromtimestamp(results_part_one.timestamp), results_part_one.height))
    print('Part 2 - Previous 10 changes in difficulty:')
    for result in results_part_two:
        print('Time: {}   | Block: {}   | Difficulty: {}'.format(datetime.fromtimestamp(result.timestamp), result.height, result.difficulty))
    print('Average time between difficulty changes: {}.'.format(avg_time_difference))
    print('-------------------------------------------------------------\n')
    ##### END: Print the results of question 10

    # Return the results
    return results_part_one, results_part_two, avg_time_difference


if __name__ == '__main__':
    # Defined in order to avoid having to run question_one()
    target_day = 1515061780

    
    # Question 1
    target_block, target_day = question_one()

    # Question 2
    result_block, list_of_transactions = question_two()

    # Question 3
    issued = question_three(target_day)

    # Question 4
    change_time = question_four()

    # Question 5
    price = question_five(target_block)

    # Question 6
    decreased_block, previous_block = question_six(target_block)

    # Question 7
    before, after = question_seven(target_block)

    # Question 8
    longest, longest_blocks, shortest, shortest_blocks = question_eight(target_day)

    # Question 9
    maximum_fee, minimum_fee, largest_transaction_fee = question_nine(target_block)
    
    # Question 10
    results_part_one, results_part_two, avg_time_difference = question_ten(target_day)
