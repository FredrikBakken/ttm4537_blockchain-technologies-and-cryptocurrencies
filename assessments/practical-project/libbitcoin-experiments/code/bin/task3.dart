import 'package:Libbitcoin_Experiments/base.dart' as b;
import 'package:Libbitcoin_Experiments/classes.dart' as c;
import 'package:Libbitcoin_Experiments/settings.dart' as s;
import 'package:Libbitcoin_Experiments/sendBTC.dart' as sBTC;


void execute_task3() async {
  // Initialize variables
  int amount = 90;
  int fee = 10;
  bool percent_paying = true;

  // Create a multisignature address
  List<String> multisig_cmd = ['script-to-address', '2 [' + s.wallet_1.public_key + '] [' + s.wallet_2.public_key + '] 2 checkmultisig'];
  String multisig_address = await b.execute_process(multisig_cmd);
  c.Multisig multisig = c.Multisig(multisig_address, multisig_cmd);

  // Printing to console
  print('\n\nTask 3: Using multisignatures with P2SH');
  print('Public key 1: ' + s.wallet_1.public_key);
  print('Public key 2: ' + s.wallet_2.public_key);
  print('Multisignature address: ' + multisig_address);

  // Send OP RETURN transaction
  await sBTC.fetchHistory(multisig);
  await sBTC.sendBTC(s.wallet_1, s.wallet_2, amount, fee, percent_paying, execute_tx: false, basic_payment: false, op_return: false, multisignatures: true, ntxs: 1, multisig: multisig);
}