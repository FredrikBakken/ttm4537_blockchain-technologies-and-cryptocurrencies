# TTM4537: Bitcoin Blockchain Exploration

## Feedback from Lecturer

```
Very nice report. Two minor comments:

- Q7. Probably a more significant reason for the variability in inter-block time is the randomness in the process.

- Q9. I think here you found the largest total amount of tx fee in a block. The largest single tx fee was in block 512115.
```

## Installation and Execution

In order to execute the developed application, follow these pre-requirement steps:

1. Install Python 3.6 (used during development phase)
2. Install the requirements: ``` python pip install -r requirements.txt ```

Execute the source code (estimated runtime: 6min and 20sec) by: ``` python main.py ```

## Background

This assignment consists of practical exploration of the Bitcoin blockchain. It should help you to consolidate your understanding of the main parameters of Bitcoin and how they can vary over time. We will be discussing all of the parameters mentioned in the task during the lectures, mostly in weeks 3 and 4 of the course.

It is expected that you will make use of one or more Bitcoin explorers to perform the experiments. Specific interfaces that may be useful include:

 - The charts (https://www.blockchain.com/charts) and statistics (https://www.blockchain.com/stats) at Blockchain.com
 - The API interface to the BTC.com explorer: https://btc.com/api-doc
 - The configurable explorer at https://blockchair.com

You should not need any special software for this practical task but use of a spreadsheet tool such as Excel may be helpful and you are free to use any other software or to write your own scripts or programs.

You should deliver a report by the deadline. For each experiment, describe what you found and explain, in a few sentences, why you think these results occurred given what we have studied in the lectures. This is an individual assignment. It is acceptable
to discuss the general approach with your fellow students and the unit staff, but your answers should be your own.

There are 10 marks available in total for this assignment, one point for each question. Your report must be submitted by the due date of 21 September 2018. Submission should be made through Blackboard.