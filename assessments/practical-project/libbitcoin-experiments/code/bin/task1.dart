import 'package:Libbitcoin_Experiments/classes.dart' as c;
import 'package:Libbitcoin_Experiments/settings.dart' as s;
import 'package:Libbitcoin_Experiments/sendBTC.dart' as sBTC;


void execute_task1() async {
  // Initialize variables
  int amount;
  int fee;
  bool percent_paying = s.percent_paying;   // (default: true)

  // Define amount and fee to transfer
  if (percent_paying) {
    amount  = s.percent_paying_amount;
    fee     = s.percent_paying_fee;
  } else {
    amount  = s.paying_amount;
    fee     = s.paying_fee;
  }

  // Printing to console
  print('\nTask 1: Basic Payments');

  // Transfer BTC from Address 1 to Address 2
  c.Wallet sender   = s.wallet_1;
  c.Wallet receiver = s.wallet_2;

  print('Transaction 1:');
  print('Sending from address: ' + sender.address);
  print('Sending to address:   ' + receiver.address);

  await sBTC.fetchHistory(sender);
  await sBTC.sendBTC(sender, receiver, amount, fee, percent_paying, execute_tx: false, basic_payment: true, op_return: false, multisignatures: false, ntxs: 1, encoded_script: null);

  // Transfer BTC from Address 2 to Address 1
  sender    = s.wallet_2;
  receiver  = s.wallet_1;

  print('\nTransaction 2:');
  print('Sending from address: ' + sender.address);
  print('Sending to address:   ' + receiver.address);

  await sBTC.fetchHistory(sender);
  await sBTC.sendBTC(sender, receiver, amount, fee, percent_paying, execute_tx: false, basic_payment: true, op_return: false, multisignatures: false, ntxs: 1, encoded_script: null);
}
