import 'package:Libbitcoin_Experiments/settings.dart' as s;
import 'package:Libbitcoin_Experiments/generate_wallet.dart' as g;


void execute_task0() async {
  // Initialize the seeds (change with your seeds)
  String seed_1 = '8bbe10a6109efb14d1f58da1360c20a6c4bc5b16ab5ad707'; // 8bbe10a6109efb14d1f58da1360c20a6c4bc5b16ab5ad707
  String seed_2 = '10d2ce5779d45fc2c9dee254485d1867504ad7295664d8c2'; // 10d2ce5779d45fc2c9dee254485d1867504ad7295664d8c2

  // Set the sender and receiver
  s.wallet_1  = await g.generate_wallet(seed_1, s.group_number, use_group_number: true);
  s.wallet_2  = await g.generate_wallet(seed_2, s.group_number, use_group_number: true);

  // Print the results to the console
  print('\nTask 0: Key and Address Setup');
  print('Wallet 1:');
  print('Seed        | ' + s.wallet_1.seed);
  print('Private key | ' + s.wallet_1.private_key);
  print('Public key  | ' + s.wallet_1.public_key);
  print('Address     | ' + s.wallet_1.address + '\n');
  print('Wallet 2:');
  print('Seed        | ' + s.wallet_2.seed);
  print('Private key | ' + s.wallet_2.private_key);
  print('Public key  | ' + s.wallet_2.public_key);
  print('Address     | ' + s.wallet_2.address + '\n');
}
