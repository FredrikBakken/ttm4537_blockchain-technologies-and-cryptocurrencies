# TTM4537: Bitcoin Transactions (Libbitcoin)

## Installation of Bitcoin Explorer (Libbitcoin)

**Important to note:** There is currently a bx.exe file in the 'code/bin/bx'-folder, make sure to replace this file with your own.

1. **Option 1:** Download the binary copy for your specific OS. It is a single executable file and can be found at: https://github.com/libbitcoin/libbitcoin-explorer/wiki/Download-BX (Windows: Make sure to rename the file to bx.exe).

2. **Option 2:** Build the executable file from the source. For Windows, follow the guides found at: https://github.com/libbitcoin/libbitcoin-explorer#windows. Each build process will generate a new bin-folder with the result files (in the libbitcoin-explorer, the bx.exe executable will be found).

Once the executable is successfully downloaded or built, make sure that it is running as expected:

1. Open your default terminal (cmd on Windows).
2. ```cd``` into the folder where the bx executable is located.
3. Run (Windows) ```bx.exe``` to get all available bx commands.

## Installation and Execution of Project Files

1. Install the command-line version of Dart: https://www.dartlang.org/tools/sdk#install

2. Execute the source code by running: ```dart code/bin/main.dart```

## Background

This assignment requires you to generate Bitcoin addresses, obtain Bitcoin (testnet) value, construct transactions and post them onto the Bitcoin testnet. It will also require construction of very simple scripts. The assignment should help you to understand Bitcoin transactions and scripts. This assignment should be done in groups of 2 (pairs). Your partner can be found via Blackboard.

The experiments will make use of the Bitcoin testnet. This operates in essentially the same way as the main Bitcoin network but at a smaller scale. There are some mechanisms to prevent the difficulty getting too big, but usually mining on the testnet is not possible without dedicated hardware. Test Bitcoins (tBTC) are available from different faucets which give away coins free on a limited basis. The balance of the faucets changes over time so you may need to wait to get enough balance. You may also have to wait for the payment to your address to be confirmed. Faucets you can use include:

 - http://bitcoinfaucet.uo1.net
 - https://testnet.coinfaucet.eu/en/

It is expected that you will make use of the libbitcoin software tool (https://github.com/libbitcoin/libbitcoin-explorer/wiki) to perform the experiments. This software is installed in the computers in the Sahara lab but you may choose to install and run it on your own computer. This is open source software and I have no reason to believe it will cause problems, but I cannot guarantee anything about its behaviour. You may like to consider installing it in a virtual machine.

You are free to write your own scripts to enhance the libbitcoin commands. Libbitcoin has very helpful documentation and tutorial pages. Working through the tutorial pages will guide you to complete the two tasks. The software supports the Bitcoin testnet, but you will have to set up the configuration file properly to ensure that you interact with the the testnet: https://github.com/libbitcoin/libbitcoin-explorer/ wiki/Testnet-Support.