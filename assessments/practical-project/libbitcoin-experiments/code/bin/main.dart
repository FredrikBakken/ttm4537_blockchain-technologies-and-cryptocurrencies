import 'task0.dart';
import 'task1.dart';
import 'task2.dart';
import 'task3.dart';


void main() async {
  // Task 0: Key and Address Setup
  await execute_task0();

  // Task 1: Basic Payments
  await execute_task1();

  // Task 2: Posting data using OP RETURN
  await execute_task2();

  // Task 3: Using multisignatures with P2SH
  await execute_task3();
}
