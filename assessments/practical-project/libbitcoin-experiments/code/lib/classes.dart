class Wallet {
  String seed;
  String private_key;
  String public_key;
  String address;
  List<Transaction> txs;

  Wallet(String seed, String private_key, String public_key, String address) {
    this.seed = seed;
    this.private_key = private_key;
    this.public_key = public_key;
    this.address = address;
  }

  void updateTxs(List<Transaction> txs) {
    this.txs = txs;
  }
}


class Multisig {
  String address;
  List<String> multisig_cmd;
  List<Transaction> txs;

  Multisig(String address, List<String> multisig_cmd) {
    this.address = address;
    this.multisig_cmd = multisig_cmd;
  }

  void updateTxs(List<Transaction> txs) {
    this.txs = txs;
  }
}


class Transaction {
  String tx_hash;
  int height;
  int index;
  int value;

  Transaction(String tx_hash, int height, int index, int value) {
    this.tx_hash = tx_hash;
    this.height = height;
    this.index = index;
    this.value = value;
  }
}


class Output {
  String address_hash;
  String script;
  int value;

  Output(String address_hash, String script, int value) {
    this.address_hash = address_hash;
    this.script = script;
    this.value = value;
  }
}
