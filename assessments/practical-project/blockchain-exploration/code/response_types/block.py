
# Class: Block
class Block(object):
    """ Class for representing a Bitcoin block
    """

    # Class initializer
    def __init__(self, bits, confirmations, created_at,
                 curr_max_timestamp, difficulty, extras, bhash,
                 height, is_orphan, is_sw_block, mrkl_root,
                 next_block_hash, nonce, pool_difficulty,
                 prev_block_hash, reward_block, reward_fees,
                 size, stripped_size, timestamp, tx_count,
                 version, weight):

        self.bits               = bits
        self.confirmations      = confirmations
        self.created_at         = created_at
        self.curr_max_timestamp = curr_max_timestamp
        self.difficulty         = difficulty
        self.extras             = extras
        self.hash               = bhash
        self.height             = height
        self.is_orphan          = is_orphan
        self.is_sw_block        = is_sw_block
        self.mrkl_root          = mrkl_root
        self.next_block_hash    = next_block_hash
        self.nonce              = nonce
        self.pool_difficulty    = pool_difficulty
        self.prev_block_hash    = prev_block_hash
        self.reward_block       = reward_block
        self.reward_fees        = reward_fees
        self.size               = size
        self.stripped_size      = stripped_size
        self.timestamp          = timestamp
        self.tx_count           = tx_count
        self.version            = version
        self.weight             = weight


def new_block(data):
    """ Format the JSON-data and create a new Block object.
    """

    # Define a new Bitcoin block
    block = Block(
        bits                = data['bits'],
        confirmations       = data['confirmations'],
        created_at          = data['created_at'],
        curr_max_timestamp  = data['curr_max_timestamp'],
        difficulty          = data['difficulty'],
        extras              = data['extras'],
        bhash               = data['hash'],
        height              = data['height'],
        is_orphan           = data['is_orphan'],
        is_sw_block         = data['is_sw_block'],
        mrkl_root           = data['mrkl_root'],
        next_block_hash     = data['next_block_hash'],
        nonce               = data['nonce'],
        pool_difficulty     = data['pool_difficulty'],
        prev_block_hash     = data['prev_block_hash'],
        reward_block        = data['reward_block'],
        reward_fees         = data['reward_fees'],
        size                = data['size'],
        stripped_size       = data['stripped_size'],
        timestamp           = data['timestamp'],
        tx_count            = data['tx_count'],
        version             = data['version'],
        weight              = data['weight']
    )
    
    # Return the Bitcoin block
    return block
